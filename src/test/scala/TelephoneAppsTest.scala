import java.io.ByteArrayInputStream
import java.nio.channels.Channels

import org.itplatform.exam._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.WordSpecLike

import scala.util.{Failure, Success}

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 *         30.04.2015 19:42
 */
class TelephoneAppsTest extends WordSpecLike with Matchers with BeforeAndAfterAll {
  import BaseDictionary._

  "TelephonesApp" must {
    "return SingleEntryFound if lookup() method resolved a single matching entry" in {
      val telephonesApp = new TelephonesApp(
        Channels.newChannel(
          new ByteArrayInputStream("coffee\ncappuccino\ncinnamon".getBytes)
        ), new CSVDictionaryLoader()
      )

      val coffee = "coffee"
      telephonesApp.process(coffee.toDigits.toLong) match {
        case Success(r) => r should equal(SingleEntryFound(coffee))

        case Failure(e) => fail(e.getMessage, e)
      }
    }
    "also return a SingleEntryFound if lookup() received part of a dictionary word digitized form and only one entry found" in {
      val telephonesApp = new TelephonesApp(
        Channels.newChannel(
          new ByteArrayInputStream("coffee\ncappuccino\ncinnamon".getBytes)
        ), new CSVDictionaryLoader()
      )

      val coffee = "coffee"
      telephonesApp.process(coffee.toDigits.toLong) match {
        case Success(SingleEntryFound("coffee")) =>
        case Success(r) => fail(s"Unexpected result $r, expected $coffee")
        case Failure(e) => fail(e.getMessage, e)
      }
    }
    "return MultipleEntryFound if lookup() method resolved multiple entries matching an input query" in {
      val cinnamon = "cinnamon"
      val chocolate = "chocolate"

      val telephonesApp = new TelephonesApp(
        Channels.newChannel(
          new ByteArrayInputStream("chocolate\ncinnamon".getBytes)
        ), new CSVDictionaryLoader()
      )

      val input = 626
      telephonesApp.process(input) match {
        case Success(MultipleEntriesFound(r)) => r should contain theSameElementsAs Array(cinnamon, chocolate)
        case Success(r) => fail(s"MultipleEntriesFound expected instead of $r")
        case Failure(e) => fail(e.getMessage, e)
      }
    }
    "return NoEntriesFound if lookup() method has not resolved any entries which match an input query" in {
      val telephonesApp = new TelephonesApp(
        Channels.newChannel(
          new ByteArrayInputStream("chocolate\ncinnamon".getBytes)
        ), new CSVDictionaryLoader()
      )

      val input = 999999999999L
      telephonesApp.process(input) match {
        case Success(NoEntriesFound) =>
        case Success(r) => fail(s"NoEntriesFound expected instead of $r")
        case Failure(e) => fail(e.getMessage, e)
      }
    }
  }

}
