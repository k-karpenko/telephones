package org.itplatform.exam

import scala.util.Try

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 *         30.04.2015 18:50
 */
trait Dictionary {

  def size(): Int

  def lookup( digits: Long ): Array[String]

}

object BaseDictionary {
  val digitalIndex = Map(
    ' ' -> 1,
    'A' -> 2, 'B' -> 2, 'C' -> 2,
    'D' -> 3, 'E' -> 3, 'F' -> 3,
    'G' -> 4, 'H' -> 4, 'I' -> 4,
    'J' -> 5, 'K' -> 5, 'L' -> 5,
    'M' -> 6, 'N' -> 6, 'O' -> 6,
    'P' -> 7, 'Q' -> 7, 'R' -> 7, 'S' -> 7,
    'T' -> 8, 'U' -> 8, 'V' -> 8,
    'W' -> 9, 'X' -> 9, 'Y' -> 9, 'Z' -> 9
  )

  implicit class BaseDictionaryOps(val str: String) {
    def toDigits: String = {
      val arr = str.toUpperCase.toCharArray map { c =>
        digitalIndex.get(c) match {
          case Some(code) => code
          case None => throw new IllegalArgumentException("unsupported character code: " + c)
        }
      }

      arr.mkString("")
    }
  }

}

class BaseDictionary(words: Array[String]) extends Dictionary {
 import BaseDictionary._

  val wordsIndex = {
    var i = 0
    words map { w =>
      val r = (i, w.toDigits)
      i += 1
      r
    }
  }


  override def size(): Int = words.length

  override def lookup(digits: Long): Array[String] = (wordsIndex map { w =>
    if ( w._2.contains(digits.toString) ) {
      Some(words(w._1))
    } else {
      None
    }
  }).flatten

}