package org.itplatform.exam

import java.nio.channels.ReadableByteChannel
import java.nio.file.{Paths, Files}
import java.util

import scala.util.Try

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 *         30.04.2015 18:13
 */
sealed trait LookupResult
case object NoEntriesFound extends LookupResult
case class SingleEntryFound(entry: String) extends LookupResult
case class MultipleEntriesFound(entry: Array[String]) extends LookupResult

class TelephonesApp(source: ReadableByteChannel, val loader: DictionaryLoader) {

  val dictionary = loader.load(source)

  def process( input: Long ): Try[LookupResult] =
    dictionary map { dict =>
      val words = dict.lookup(input)
      if ( words.isEmpty ) {
        NoEntriesFound
      } else if ( words.length > 1 ) {
        MultipleEntriesFound(words)
      } else {
        SingleEntryFound(words(0))
      }
    }
}