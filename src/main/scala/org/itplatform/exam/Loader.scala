package org.itplatform.exam

import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer
import java.nio.channels.ReadableByteChannel

import scala.collection.mutable
import scala.util.Try

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 *         30.04.2015 18:48
 */
trait DictionaryLoader {
  def load(source: ReadableByteChannel): Try[Dictionary]
}

class CSVDictionaryLoader(newLineChar: Char = '\n',
                          startLine: Int = 0, endLine: Int = -1) extends DictionaryLoader {
  override def load(source: ReadableByteChannel) = {
    val os = new ByteArrayOutputStream()
    val readBuff = ByteBuffer.allocate(256)

    var read = 0
    do {
      read = source.read(readBuff)
      readBuff.flip()
      if (readBuff.remaining() > 0) {
        os.write(readBuff.array(), 0, readBuff.remaining())
      }
      readBuff.clear()
    } while (read != -1)

    parseEntities(new String(os.toByteArray, "UTF-8"))
  }

  protected def parseEntities(data: String): Try[Dictionary]= Try {
    var lineNum = 0
    val loaded = (data.split(newLineChar) map { line =>
      val result = if (!line.isEmpty && lineNum >= startLine && (lineNum < endLine || endLine < 0)) {
        Some(line)
      } else {
        None
      }

      lineNum += 1

      result
    }).flatten

    new BaseDictionary(loaded)
  }
}

