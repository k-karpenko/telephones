package org.itplatform.exam

import java.nio.file.{Paths, Files}

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 *         30.04.2015 19:40
 */
object Main {

  val app: TelephonesApp = new TelephonesApp(
    Files.newByteChannel(Paths.get("src/test/resources/words.txt")),
    new CSVDictionaryLoader()
  )

  def main(args: Array[String]): Unit = {
    if ( args.length == 0 ) {
      System.err.println("No input provided")
      System.exit(1)
    }

    val input = args(0).toLong
    app.process( input ) map {

      case MultipleEntriesFound(entries) =>
        println(s"$input is related to several records from the dictionary: " )
        var line = 1
        entries foreach { w =>
          println(s"$line - $w")
          line += 1
        }

      case SingleEntryFound(entry) =>
        println(s"$input is related to the: " + entry )

      case NoEntriesFound =>
        println(s"No entries found for an input $input")

    } recover {
      case e: Throwable =>
        println(s"Failed to load dictionary data: ${e.getMessage}")
        e.printStackTrace()
    }
  }

}